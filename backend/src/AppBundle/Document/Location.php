<?php
/**
 * Created by PhpStorm.
 * User: khalid
 * Date: 12/16/2017
 * Time: 1:40 AM
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class Location
{
    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $type;

    /**
     * @var array
     * @MongoDB\Field(type="collection")
     */
    protected $coordinates;

    /**
     * Set type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set coordinates
     *
     * @param array $coordinates
     * @return $this
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * Get coordinates
     *
     * @return array $coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }
}
