<?php
/**
 * Created by PhpStorm.
 * User: khalid
 * Date: 12/16/2017
 * Time: 3:59 PM
 */

namespace AppBundle\Document;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @MongoDB\Document(collection="users", repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var ArrayCollection
     * @MongoDB\ReferenceMany(targetDocument="Shop", storeAs="id")
     */
    private $preferredShops;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="DislikedShop")
     */
    private $dislikedShops;

    public function __construct()
    {
        parent::__construct();
        $this->preferredShops = new ArrayCollection();
        $this->dislikedShops = new ArrayCollection();
    }

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * Add preferredShop
     *
     * @param Shop $preferredShop
     */
    public function addPreferredShop(Shop $preferredShop)
    {
        $this->preferredShops[] = $preferredShop;
    }

    /**
     * Remove preferredShop
     *
     * @param Shop $preferredShop
     */
    public function removePreferredShop(Shop $preferredShop)
    {
        $this->preferredShops->removeElement($preferredShop);
    }

    /**
     * Get preferredShops
     *
     * @return ArrayCollection $preferredShops
     */
    public function getPreferredShops()
    {
        return $this->preferredShops;
    }

    /**
     * Add dislikedShop
     *
     * @param DislikedShop $dislikedShop
     */
    public function addDislikedShop(DislikedShop $dislikedShop)
    {
        $this->dislikedShops[] = $dislikedShop;
    }

    /**
     * Remove dislikedShop
     *
     * @param DislikedShop $dislikedShop
     */
    public function removeDislikedShop(DislikedShop $dislikedShop)
    {
        $this->dislikedShops->removeElement($dislikedShop);
    }

    /**
     * Get dislikedShops
     *
     * @return ArrayCollection $dislikedShops
     */
    public function getDislikedShops()
    {
        return $this->dislikedShops;
    }
}
