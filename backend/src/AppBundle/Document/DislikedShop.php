<?php
/**
 * Created by PhpStorm.
 * User: khalid
 * Date: 12/16/2017
 * Time: 1:40 AM
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use MongoDate;

/**
 * @MongoDB\EmbeddedDocument
 */
class DislikedShop
{
    /**
     * @var Shop
     * @MongoDB\ReferenceOne(targetDocument="Shop", storeAs="id")
     */
    private $shop;

    /**
     * @var array
     * @MongoDB\Field(type="date")
     */
    private $time;

    /**
     * DislikedShop constructor.
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        $this->time = new \MongoDate();
        $this->shop = $shop;
    }

    /**
     * Set shop
     *
     * @param Shop $shop
     * @return $this
     */
    public function setShop(Shop $shop)
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * Get shop
     *
     * @return Shop $shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set time
     *
     * @param MongoDate $time
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * Get time
     *
     * @return MongoDate $time
     */
    public function getTime()
    {
        return $this->time;
    }
}
