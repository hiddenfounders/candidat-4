<?php

namespace AppBundle\Controller;

use AppBundle\Document\DislikedShop;
use AppBundle\Document\Shop;
use AppBundle\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends FOSRestController
{

    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * UsersController constructor.
     * @param DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    /**
     * @View()
     * @param $id
     * @return Response
     */
    public function getUserShopsAction($id) {
        /** @var User $user */
        $user = $this->documentManager->getRepository('AppBundle:User')->find($id);

        if (count($user->getPreferredShops()) > 0) {
            $view = $this->view($user->getPreferredShops(), 200);
            return $this->handleView($view);
        }
    }

    /**
     * @View()
     * @param $user_id
     * @param $shop_id
     */
    public function patchUserShopLikeAction($user_id, $shop_id) {
        /** @var User $user */
        $user = $this->documentManager->getRepository('AppBundle:User')->find($user_id);
        /** @var Shop $shop_id */
        $shop = $this->documentManager->getRepository('AppBundle:Shop')->find($shop_id);

        $user->addPreferredShop($shop);
        $this->documentManager->flush();
    }

    /**
     * @View()
     * @param $user_id
     * @param $shop_id
     */
    public function patchUserShopDislikeAction($user_id, $shop_id) {
        /** @var User $user */
        $user = $this->documentManager->getRepository('AppBundle:User')->find($user_id);
        /** @var Shop $shop_id */
        $shop = $this->documentManager->getRepository('AppBundle:Shop')->find($shop_id);

        $user->addDislikedShop(new DislikedShop($shop));
        $this->documentManager->flush();
    }

    /**
     * @View()
     * @param $user_id
     * @param $shop_id
     */
    public function deleteUserShopLikeAction($user_id, $shop_id) {
        /** @var User $user */
        $user = $this->documentManager->getRepository('AppBundle:User')->find($user_id);
        /** @var Shop $shop_id */
        $shop = $this->documentManager->getRepository('AppBundle:Shop')->find($shop_id);

        $user->removePreferredShop($shop);
        $this->documentManager->flush();
    }

    public function getUserAction($id)
    {}
}
