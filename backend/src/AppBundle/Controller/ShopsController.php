<?php

namespace AppBundle\Controller;

use AppBundle\Document\Shop;
use AppBundle\Document\User;
use Doctrine\ODM\MongoDB\MongoDBException;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

class ShopsController extends FOSRestController
{

    /**
     * @View()
     * @param $user_id
     * @return Response
     * @throws MongoDBException
     */
    public function getShopsAction($user_id)
    {
        /** @var User $user */
        $user = $this->get('doctrine_mongodb')->getRepository('AppBundle:User')->find($user_id);
        /** @var Shop $shop_id */
        $shops = $this->get('doctrine_mongodb')->getRepository('AppBundle:Shop')->findAllByDisliked($user);

        $view = $this->view($shops, 200);
        return $this->handleView($view);
    }
}
